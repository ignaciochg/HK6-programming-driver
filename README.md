# HK 6 USB Keyboard

This is the programming driver for the [HK6 6 button keyboard](https://www.pcsensor.com/hot-keys-6-keys-switch-shortcut-keyboard-for-win7-syetem-usb-dedicated-shortcut-keys-for-pc-hk-6.html)

![HK6 Keyboard Image](https://www.pcsensor.com/images/cache/HK_6/HK_6_MainPicture2.600.jpg "HK-6") 
